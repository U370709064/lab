public class FindLetterGrade {

	public static void main(String[] args){
	    int mark= Integer.parseInt(args[0]);
	   
      if( mark <= 100 && mark >= 90 ){
        System.out.println("Your grade is A");
      }
      if( mark < 90 && mark >= 80){
        System.out.println("Your grade is B");
      }
      if( mark < 80 && mark >= 70){
        System.out.println("Your grade is C");
      }
      if( mark < 70 && mark >= 60){
        System.out.println("Your grade is D");
      }
      if( mark <60 && mark >= 0){
        System.out.println("Your grade is F");
      }
      if( mark < 0 || mark > 100){
        System.out.println("It is not a valid score!");
       }

	}
}
