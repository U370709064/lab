import javax.lang.model.element.Element;

public class MyDate{
    public int day;
    public int month;
    public int year;

    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public String toString(){
        return year + "-"+(month + 1 < 10 ? "0" : "") + (month + 1) + "-" +(day < 10 ? "0" : "") + day; 
    }
    
    public boolean leapYear(){
        return year % 4 == 0 ? true : false;
    }

    public void incrementDay(){
        int newday = this.day + 1;
        if (newday > maxDays[month]){
            day = 1;
            incrementMonth();
        }
        else if (month == 1 && newday == 29 && !leapYear()){
            day = 1;
            incrementMonth();
        }
        else{
            day = newday;
        }

    }

    public void incrementDay(int a){
        while(a > 0){
            incrementDay();
            a--;
        }
    }

    public void incrementYear(){
        incrementYear(1);
    }

    public void incrementYear(int a){
        year += a;
        if(month == 1 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void decrementDay(){
        int newday = day - 1;
        if(newday == 0){
            day = 31;
            decrementMonth();
        }
        else{
            day = newday;
        }
    }

    public void decrementDay(int a){
        while(a > 0){
            decrementDay();
            a--;
        }
    }

    public void decrementYear(){
        incrementYear(-1);
    }

    public void decrementYear(int a){
       incrementYear(-a);
    }

    public void decrementMonth(){
        incrementMonth(-1);
    }

    public void decrementMonth(int a){
       incrementMonth(-a);
    }

    public void incrementMonth(){
       incrementMonth(1);
    }

    public void incrementMonth(int a){
        int newmonth = (month + a) % 12;
        int yearchange = 0;

        if(newmonth < 0){
            newmonth += 12;
            yearchange = -1;
        }

        yearchange += (month + a) / 12;
        month = newmonth;
        year += yearchange;
        if(day > maxDays[month]){
            day = maxDays[month];
            if(month == 1 && day == 29 && !leapYear()){
                day = 28;
            }
        }
    }


    public boolean isBefore(MyDate date){
       int a = Integer.parseInt(toString().replaceAll("-", ""));
       int b = Integer.parseInt(date.toString().replaceAll("-", ""));

       return a < b;
    }

    public boolean isAfter(MyDate date){
        int a = Integer.parseInt(toString().replaceAll("-", ""));
        int b = Integer.parseInt(date.toString().replaceAll("-", ""));

        return a > b;
    }

    public int dayDifference(MyDate date){
        int diff = 0;
        if(isBefore(date)){
            MyDate date2 = new MyDate(day, month+1, year);
            while(date2.isBefore(date)){
                date.incrementDay();
                diff++;
            }
        }
        else if(isAfter(date)){
            MyDate date2 = new MyDate(day, month+1, year);
            while(date2.isAfter(date)){
                date2.decrementDay();
                diff++;
            }
        }
        return diff;
    }
}