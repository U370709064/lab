public class Main {

    public static void main(String[] args) {
        Rectangle rectangle;
        rectangle = new Rectangle(5, 5, new Point(10, 10));
        System.out.println("area= " + rectangle.area());
        System.out.println("perimeter= " + rectangle.perimeter());
        System.out.println("corners= ");

        for (Point p : rectangle.corners()) {
            System.out.println("(" + p.xCoord + "," + p.yCoord + ")");
        }


        Circle circle;
        circle = new Circle(10, new Point(10, 10));
        System.out.println("area= " + circle.area());
        System.out.println("perimeter= " + circle.perimeter());
        Circle circle1 = new Circle(4, new Point(17, 10));
        System.out.println(circle.intersect(circle1));
        Circle circle2 = new Circle(4, new Point(25, 10));
        System.out.println(circle.intersect(circle2));

    }

}